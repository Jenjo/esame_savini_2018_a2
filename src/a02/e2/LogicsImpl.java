package a02.e2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class LogicsImpl implements Logics {
	
	private final String fileName;
	private List<String> list = new ArrayList<>();
	
	public LogicsImpl(String s) throws IOException {
		this.fileName = s;
		this.initialize();;
	}
	
	private void initialize() throws FileNotFoundException {
		//Creato Lettore di un file
		BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
		try {
			//Salvo in una stringa cio che c'è nella prima riga.
			String str = bufferedReader.readLine();
			//ciclo finche non trovo una riga vuota (stringa = null)
			while(str != null) {
				//aggiungo la stringa alla lista
				this.list.add(str);
				//aggiorno l'elemento da aggiungere alla lista
				str = bufferedReader.readLine();
			}
			//chiudo tutto
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<String> allLines() {
		return new ArrayList<>(this.list);
	}

	@Override
	public void remove(int index) {
		//apro il file e lo sovrascrivo
		try (PrintStream ps = new PrintStream(fileName)) {
			for (String str : this.list) {
				if (!str.equals(this.list.get(index))) {
					//Scrivo il file riga per riga
					ps.println(str);
					System.out.println(str);
				}
			}
			//alla fine elimino la riga da rimuovere
			this.list.remove(index);
			System.out.println();
		} catch (IOException e) {
			throw new IllegalStateException();
		}
	}

	@Override
	public void add(int index) {
		//apro il file e lo sovrascrivo
		try (PrintStream ps = new PrintStream(fileName)) {
			String s="*";
			for (String str : this.list) {
				//Se trovo la linea a cui aggiungere l'asterisco...
				if (str.equals(this.list.get(index))) {
					//accodo la stringa all'asterisco
					s += str;
					ps.print(s);
					System.out.println(s);
				}
				ps.println(str);
				System.out.println(str);
			}
			//lo metto dopo la linea corrente
			this.list.add(index + 1, s);
			System.out.println();
		} catch (IOException e) {
			throw new IllegalStateException();
		}
	}

	@Override
	public void concat(int index) {
		try (PrintStream ps = new PrintStream(fileName)) {
			String s="";
			for (String str : this.list) {
				if (str.equals(this.list.get(index))) {
					s = str + str;
					ps.print(s);
					System.out.println(s);
				} else {
					ps.println(str);
				}
				System.out.println(str);
			}
			//Lo rimuovo e addo quello modificato
			this.list.remove(index);
			this.list.add(index, s);
			System.out.println();
		} catch (IOException e) {
			throw new IllegalStateException();
		}
	}
}
